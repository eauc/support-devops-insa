#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/htmlize.css"/>
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="http://www.pirilampo.org/styles/readtheorg/css/readtheorg.css"/>
#+HTML_HEAD: <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
#+HTML_HEAD: <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/lib/js/jquery.stickytableheaders.js"></script>
#+HTML_HEAD: <script type="text/javascript" src="http://www.pirilampo.org/styles/readtheorg/js/readtheorg.js"></script>
#+HTML_HEAD: <style>pre.src { color: white; background-color: #333 }</style>

* TD Devops

** Projet de démarrage

*** Forker/cloner le repository Git de base

- créer un compte sur gitlab.com
- aller sur le projet https://gitlab.com/eauc/support-devops-insa.git
- clicker sur le bouton "Fork" pour copier le projet dans votre espace perso gitlab.
- cloner votre projet perso sur votre machine de dev.

#+BEGIN_SRC sh
git clone <url du projet>
#+END_SRC

*** Rappels quelques commandes Git

Dans le répertoire du projet
#+BEGIN_SRC sh
git status # voir les modifications locales des fichiers du projet
git commit -a # creer un nouveau commit avec toutes les modification locales
git push origin <branch> # pousser les nouveaux commits crees localement sur le repository gitlab
git pull # recuperer les nouveaux commits sur le serveur gitlab
git <cmd> --help # aide pour n'importe quelle commande git
#+END_SRC
** Docker sur le poste de developpement

*** Installer Docker CE

Exemple sur une Debian:
[[https://docs.docker.com/engine/installation/linux/docker-ce/debian/][Doc officielle]]
*** Image node de base

TODO:
- aller sur le [[https://hub.docker.com/][Docker Hub]].
- trouver l'image officielle Node.
- créer un fichier Dockerfile dans le projet, basé sur l'image officielle node pour la version spécifiée dans les tests.
- exécuter un shell node dans un container basé sur cette image.
- vérifier que la version de Node est bien celle désirée.

Directives Dockerfile:
- =FROM <image:version>= :: base l'image construite sur une autre image Docker.

Commandes docker:
- =docker build <dir>= :: construit une image docker à partir du Dockerfile situé dans un répertoire.
   - l'option =-t= :: permet d'affecter un tag (nom) à l'image.
- =docker image ls= :: liste les images présentes sur le docker local.
- =docker image rm <image>= :: efface une image docker.
- =docker run <image>= :: crée et démarre un container base sur une image docker.
   - l'option =-i= :: (interactive) garde le stdin du container ouvert et le connecte au stdin du terminal.
   - l'option =-t= :: (tty) le stdin se comporte comme un terminal TTY.
   - l'option =--name <name>= :: nomme le container (plus pratique pour l'arrêter, etc).
   - l'option =--rm= :: efface le container lorsque son exécution se termine.
   - l'option =-d= :: (daemon) détache le container du terminal.
- =docker container ls= :: liste les containers présents sur la plateforme docker locale.
  - l'option =-a= :: liste les containers ayant terminé leur exécution.
- =docker start <container>= :: démarre un container précédemment créé par =docker run=.
- =docker stop <container>= :: arrête un container.
- =docker container rm <container>= :: efface un container.

Commande dans un shell Node:
- =process.version= :: affiche la version courante de node.

*** Image de développement

TODO:
- inclure les sources du serveur dans l'image du projet.
- exécuter un shell bash dans un container base sur l'image pour verifier que les sources sont bien incluses dans l'image.
- installer les dépendances node dans l'image du projet.
- exécuter un shell bash dans un container basé sur l'image pour vérifier que les dépendances sont bien incluses dans l'image.
- démarrer le server node automatiquement au lancement du container.
- lancer le container (sans surcharger la commande de démarrage, et en publiant le port 3000 sur localhost) pour vérifier que le server est bien démarré.
- charger [[http://localhost:3000]] dans un navigateur (ou CURL) pour vérifier que le server répond correctement.

Directives Dockerfile:
- =COPY <src> <dst>= :: copie tout le contenu du repertoire <src> de la machine hôte (relatif au Dockerfile), dans le répertoire <dst> du filesystem de l'image docker.
- =WORKDIR <dir>= :: change le répertoire courant dans le filesystem de l'image docker (~ =cd <dir>=).
  - affecte les directives suivantes du Dockerfile.
  - les commandes exécutées dans les containers seront également lancées depuis ce répertoire.
- =RUN <cmd arg...>= :: exécute une commande, durant la construction de l'image, dans le filesystem de l'image.
- =CMD ["<cmd>", "<arg1>", "<arg2>", ...]= :: définit la commande par défaut exécutée au lancement d'un container.
  - eg. =CMD ["echo", "toto"]= - il faut passer un tableau de Strings séparées par des virgules !

Commande docker:
- =docker run <image> <cmd>= :: exécute un container basé sur <image>, en remplacant la commande par défaut par <cmd>.
  - l'option =-p <port-container>:<port-hote>= :: permet de publier un port TCP du container sur la machine hôte.

Commandes node:
- =npm install= :: installe les dépendances node décrites dans le =package.json= du projet.
  - les dépendances sont installées dans le répertoire =node_modules= à la racine du projet.
- =npm start= :: démarre le serveur node.

*** Setup de developpement

TODO:
- exécuter un container de développement, en montant le répertoire du projet sur la machine hôte comme un volume dans le container, de sorte que les sources en cours d'édition soient visibles dans le container.
- lancer le server dans le container avec nodemon.
- vérifier que lorsqu'un fichier source est modifié, le cient redémarre dans le container.

Commandes docker:
- =docker run <image> <cmd>= ::
  - l'option =-v <repertoire-hote>:<repertoire-container>= :: permet de "monter" un répertoire de la machine hôte à la place d'un répertoire dans le container. Le container peut ainsi "voir" le contenu d'un répertoire de la machine hote.

Commandes node:
- =npm install --save-dev <package>= :: installe une dépendance de développement et la rajoute au =package.json= du projet.
- =./node_modules/.bin/nodemon .= :: exécute le serveur node du projet, le redémarre si une source est modifiée.

** Setup CI/CD

   Lorsque vous poussez un projet sur Gitlab.com avec un fichier =.gitlab-ci.yml=, la chaine de CI de gitlab est automatiquement lancée sur le commit poussé.

   [[https://docs.gitlab.com/ce/ci/yaml/README.html][Doc du fichier gitlab-ci]]

   Ce projet contient un embryon de fichier =.gitlab.ci.yml= à utiliser comme base de départ pour le TD. Il comporte notament les définitions de plusieurs variables d'environnement nécessaires à l'utilisation de docker dans la CI Gitlab.

   Le fichier contient plusieurs sections:

*** Stages

    On peut définir plusieurs étapes qui seront exécutées séquentiellement par la CI.

    #+BEGIN_SRC yaml
    stages:
      - build
      - test
      - deploy
    #+END_SRC
    Dans cet example, chaque "pipeline" de CI comprendra 3 étapes séquentielles : build > test > deploy

*** Jobs

    Les "jobs" sont des tâches qui s'exécutent lors de chaque "stage" du pipeline. Si un job échoue, les "stages" suivants du pipeline sont ignorés et le pipeline de CI échoue à cette étape.

    On peut définir un job de la facon suivante :
    #+BEGIN_SRC yaml
    myJob:
      stage: build
      script:
        - docker build -t $CONTAINER_TEST_IMAGE .
    #+END_SRC
    Dans cet example, =myJob= sera exécuté durant l'étape de =build=. La propriété =script= est une liste de commandes docker à exécuter pour accomplir cette tâche. Ici on construit simplement une image, nommée à partir d'une variable d'environnement.

*** Heroku

    Heroku est une plateforme Saas qui permet notament de déployer des serveurs gratuitement, basés sur une image docker.

    Le déploiement consiste en 3 étapes:
    - construire une image docker exécutant le serveur.
    - pousser cette image docker dans le repository heroku =registry.heroku.com=.
    - publier ("release") cette image pour que le container serveur redémarre en l'utilisant.

**** Creer un compte Heroku

     [[https://www.heroku.com/]]

     - récupérer votre clef d'API: Accounts Settings > Account > API Key
     - la copier dans les variables d'environnement de votre projet Gitlab.com: Settings > CI/CD > Variables. L'appeler par example =HEROKU_AUTH_TOKEN=.

**** Pousser une image dans le repository Heroku

     - il faut tout d'abord "tagger" l'image avec le nom du registry Heroku, le nom de l'application visée, et le type de process - web dans notre cas.
     - ensuite il faut se login sur le registry Heroku, grâce à la clef d'API.
     - ensuite on peut simplement pousser l'image dans le repository Heroku.

     #+BEGIN_SRC sh
     docker tag <my-image> registry.heroku.com/<app-name>/web
     docker login --username=_ --password=<HEROKU_AUTH_TOKEN> registry.heroku.com
     docker push registry.heroku.com/<app-name>/web
     #+END_SRC

**** Publier l'image

     Ca s'invente pas, on peut utiliser la commande suivante:
     #+BEGIN_SRC sh
     docker run --rm -e HEROKU_API_KEY=$HEROKU_AUTH_TOKEN wingrunr21/alpine-heroku-cli container:release web --app <app-name>
     #+END_SRC

**** Voir les logs du client dans Heroku

     Dans le dashboard de l'application, en haut a droite More > View logs.

     Accès au log du client comme en local.

     On peut aussi installer l'outil Heroku CLI : [[https://devcenter.heroku.com/articles/heroku-cli]] qui permet de visualiser les logs d'une appli en ligne de commande.
     #+BEGIN_SRC sh
     heroku login
     heroku logs -t --app <app-name>
     #+END_SRC

*** Consignes

TODO:
    - compléter le fichier =.gitlab-ci.yml= pour réaliser 3 étapes : =build > test > deploy=
      - l'étape =build= doit construire une image de votre server et la pousser dans le repository gitlab (le fichier fourni initialise les variables nécessaires, et réalise le login dans ce repository).
      - l'étape =test= doit récupérer l'image construite à l'étape précédente, et exécuter les tests unitaires dans un container utilisant cette image.
      - l'étape =deploy= doit récupérer l'image, et la déployer dans Heroku comme expliqué ci-dessus.
    - tester ce déploiement continu (il est fortement conseillé de proceder par... étapes progressives).
    - une fois la CI fonctionnelle, enregistrer votre server Heroku dans le hub Heroku a cette addresse : [[https://eauc-xtrem-carpaccio.herokuapp.com/]]
    - implémenter chaque feature de l'extreme carpaccio, pour arriver à un client complètement fonctionnel.
      - faire un commit a chaque étape.
      - si vous avez le temps, écrire des tests unitaires validant chaque fonctionnalité (des tests de base existent déja dans le répertoire [[./test]]).

    Pour le rapport de TD, écrire un document expliquant à un nouveau membre d'equipe:
    - comment fonctionne la CI,
    - comment lancer un container de développement,
    - comment déployer le projet dans Gitlab.
